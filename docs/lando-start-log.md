# Start Lando

When you start Lando, you should get output something like this:

```
$ lando start
Let's get this party started! Starting app..
Recreating landoproxyhyperion5000gandalfedition_proxy_1 ... done
Creating network "landogatsbydrupal_default" with the default driver
Creating volume "landogatsbydrupal_data_appserver_nginx" with default driver
Creating volume "landogatsbydrupal_home_appserver_nginx" with default driver
Creating volume "landogatsbydrupal_data_appserver" with default driver
Creating volume "landogatsbydrupal_home_appserver" with default driver
Creating volume "landogatsbydrupal_data_database" with default driver
Creating volume "landogatsbydrupal_home_database" with default driver
Creating volume "landogatsbydrupal_data_nodejs" with default driver
Creating volume "landogatsbydrupal_home_nodejs" with default driver
Creating landogatsbydrupal_nodejs_1 ... done
Killing landogatsbydrupal_nodejs_1 ... done
Creating landogatsbydrupal_appserver_1 ... done
Killing landogatsbydrupal_appserver_1 ... done
Starting landogatsbydrupal_appserver_1 ... done
Killing landogatsbydrupal_appserver_1 ... done
Starting landogatsbydrupal_appserver_1 ... done
Creating landogatsbydrupal_appserver_nginx_1 ... done
Killing landogatsbydrupal_appserver_nginx_1 ... done
Starting landogatsbydrupal_appserver_1 ... done
Starting landogatsbydrupal_appserver_nginx_1 ... done
Killing landogatsbydrupal_appserver_nginx_1 ... done
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   599    0   599    0     0   2397      0 --:--:-- --:--:-- --:--:--  2405
100 4580k  100 4580k    0     0  3935k      0  0:00:01  0:00:01 --:--:-- 10.1M
 Drush Version   :  8.1.18

  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  713k  100  713k    0     0   570k      0  0:00:01  0:00:01 --:--:--  570k
> DrupalProject\composer\ScriptHandler::checkComposerVersion
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
    1/135:	https://codeload.github.com/minkphp/MinkGoutteDriver/legacy.zip/8b9ad6d2d95bc70b840d15323365f52fcdaea6ca
    2/135:	https://codeload.github.com/jcalderonzumba/MinkPhantomJSDriver/legacy.zip/008f43670e94acd39273d15add1e7348eb23848d
    3/135:	https://codeload.github.com/sebastianbergmann/resource-operations/legacy.zip/ce990bb21759f94aeafd30209e8cfcdfa8bc3f52
    4/135:	https://codeload.github.com/symfony/phpunit-bridge/legacy.zip/a43a2f6c465a2d99635fea0addbebddc3864ad97
    5/135:	https://codeload.github.com/justinrainbow/json-schema/legacy.zip/dcb6e1006bb5fd1e392b4daa68932880f37550d4
    6/135:	https://codeload.github.com/phpspec/prophecy/legacy.zip/4ba436b55987b4bf311cb7c6ba82aa528aac0a06
    7/135:	https://codeload.github.com/sebastianbergmann/object-enumerator/legacy.zip/7cfd9e65d11ffb5af41198476395774d4c8a84c5
    8/135:	https://codeload.github.com/jcalderonzumba/gastonjs/legacy.zip/575a9c18d8b87990c37252e8d9707b29f0a313f3
    9/135:	https://codeload.github.com/sebastianbergmann/comparator/legacy.zip/34369daee48eafb2651bea869b4b15d75ccc35f9
    10/135:	https://codeload.github.com/sebastianbergmann/diff/legacy.zip/347c1d8b49c5c3ee30c7040ea6fc446790e6bddd
    11/135:	https://codeload.github.com/sebastianbergmann/object-reflector/legacy.zip/773f97c67f28de00d397be301821b06708fca0be
    12/135:	https://codeload.github.com/bovigo/vfsStream/legacy.zip/095238a0711c974ae5b4ebf4c4534a23f3f6c99d
    13/135:	https://codeload.github.com/doctrine/instantiator/legacy.zip/a2c590166b2133a4633738648b6b064edae0814a
    14/135:	https://codeload.github.com/sebastianbergmann/exporter/legacy.zip/234199f4528de6d12aaa58b612e98f7d36adb937
    15/135:	https://codeload.github.com/sebastianbergmann/recursion-context/legacy.zip/5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8
    16/135:	https://codeload.github.com/sebastianbergmann/global-state/legacy.zip/e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4
    17/135:	https://codeload.github.com/sebastianbergmann/php-timer/legacy.zip/3dcf38ca72b158baf0bc245e9184d3fdffa9c46f
    18/135:	https://codeload.github.com/sebastianbergmann/php-text-template/legacy.zip/31f8b717e51d9a2afca6c9f046f5d69fc27c8686
    19/135:	https://codeload.github.com/sebastianbergmann/php-token-stream/legacy.zip/791198a2c6254db10131eecfe8c06670700904db
    20/135:	https://codeload.github.com/sebastianbergmann/php-file-iterator/legacy.zip/730b01bc3e867237eaac355e06a36b85dd93a8b4
    21/135:	https://codeload.github.com/sebastianbergmann/phpunit-mock-objects/legacy.zip/cd1cf05c553ecfec36b170070573e540b67d3f1f
    22/135:	https://codeload.github.com/sebastianbergmann/code-unit-reverse-lookup/legacy.zip/4419fcdb5eabb9caa61a27c7a1db532a6b55dd18
    23/135:	https://codeload.github.com/sebastianbergmann/environment/legacy.zip/cd0871b3975fb7fc44d11314fd1ee20925fce4f5
    24/135:	https://codeload.github.com/sebastianbergmann/version/legacy.zip/99732be0ddb3361e16ad77b68ba41efc8e979019
    25/135:	https://codeload.github.com/theseer/tokenizer/legacy.zip/1c42705be2b6c1de5904f8afacef5895cab44bf8
    26/135:	https://codeload.github.com/phpDocumentor/ReflectionDocBlock/legacy.zip/bdd9f737ebc2a01c06ea7ff4308ec6697db9b53c
    27/135:	https://codeload.github.com/phpDocumentor/ReflectionCommon/legacy.zip/21bdeb5f65d7ebf9f43b1b25d404f87deab5bfb6
    28/135:	https://codeload.github.com/phpDocumentor/TypeResolver/legacy.zip/9c977708995954784726e25d0cd1dddf4e65b0f7
    29/135:	https://codeload.github.com/phar-io/version/legacy.zip/a70c0ced4be299a63d32fa96d9281d03e94041df
    30/135:	https://codeload.github.com/myclabs/DeepCopy/legacy.zip/e6828efaba2c9b79f4499dae1d66ef8bfa7b2b72
    31/135:	https://codeload.github.com/phar-io/manifest/legacy.zip/2df402786ab5368a0169091f61a7c1e0eb6852d0
    32/135:	https://codeload.github.com/sebastianbergmann/phpunit/legacy.zip/bac23fe7ff13dbdb461481f706f0e9fe746334b7
    33/135:	https://codeload.github.com/minkphp/MinkBrowserKitDriver/legacy.zip/1b9a7ce903cfdaaec5fb32bfdbb26118343662eb
    34/135:	https://codeload.github.com/symfony/browser-kit/legacy.zip/c09c18cca96d7067152f78956faf55346c338283
    35/135:	https://codeload.github.com/instaclick/php-webdriver/legacy.zip/6fa959452e774dcaed543faad3a9d1a37d803327
    36/135:	https://codeload.github.com/FriendsOfPHP/Goutte/legacy.zip/3f0eaf0a40181359470651f1565b3e07e3dd31b8
    37/135:	https://codeload.github.com/minkphp/MinkSelenium2Driver/legacy.zip/8684ee4e634db7abda9039ea53545f86fc1e105a
    38/135:	https://codeload.github.com/sebastianbergmann/php-code-coverage/legacy.zip/c89677919c5dd6d3b3852f230a663118762218ac
    39/135:	https://codeload.github.com/zendframework/zend-escaper/legacy.zip/31d8aafae982f9568287cb4dce987e6aff8fd074
    40/135:	https://codeload.github.com/minkphp/Mink/legacy.zip/d5ee350c40baff5f331a05ebdbe1927345c9ac8b
    41/135:	https://codeload.github.com/dbrumann/polyfill-unserialize/legacy.zip/844d7e44b62a1a3d5c68cfb7ebbd59c17ea0fd7b
    42/135:	https://codeload.github.com/zendframework/zend-diactoros/legacy.zip/20da13beba0dde8fb648be3cc19765732790f46e
    43/135:	https://codeload.github.com/zaporylie/composer-drupal-optimizations/legacy.zip/173c198fd7c9aefa5e5b68cd755ee63eb0abf7e8
    44/135:	https://codeload.github.com/zendframework/zend-feed/legacy.zip/d926c5af34b93a0121d5e2641af34ddb1533d733
    45/135:	https://codeload.github.com/zendframework/zend-stdlib/legacy.zip/66536006722aff9e62d1b331025089b7ec71c065
    46/135:	https://codeload.github.com/vlucas/phpdotenv/legacy.zip/2a7dcf7e3e02dc5e701004e51a6f304b713107d5
    47/135:	https://codeload.github.com/TYPO3/phar-stream-wrapper/legacy.zip/b7a21f0859059ed5d9754af8c11f852d43762334
    48/135:	https://codeload.github.com/symfony/psr-http-message-bridge/legacy.zip/9ab9d71f97d5c7d35a121a7fb69f74fee95cd0ad
    49/135:	https://codeload.github.com/symfony/serializer/legacy.zip/99aceeb3e10852b951b9cab57a2b83062db09efb
    50/135:	https://codeload.github.com/squizlabs/PHP_CodeSniffer/legacy.zip/b8a7362af1cc1aadb5bd36c3defc4dda2cf5f0a8
    51/135:	https://codeload.github.com/pear/Archive_Tar/legacy.zip/7e48add6f8edc3027dd98ad15964b1a28fd0c845
    52/135:	https://codeload.github.com/symfony/class-loader/legacy.zip/4459eef5298dedfb69f771186a580062b8516497
    53/135:	https://codeload.github.com/pear/pear-core-minimal/legacy.zip/742be8dd68c746a01e4b0a422258e9c9cae1c37f
    54/135:	https://codeload.github.com/symfony-cmf/routing/legacy.zip/fb1e7f85ff8c6866238b7e73a490a0a0243ae8ac
    55/135:	https://codeload.github.com/stackphp/builder/legacy.zip/fb3d136d04c6be41120ebf8c0cc71fe9507d750a
    56/135:	https://codeload.github.com/pear/Console_Getopt/legacy.zip/6c77aeb625b32bd752e89ee17972d103588b90c0
    57/135:	https://codeload.github.com/egulias/EmailValidator/legacy.zip/709f21f92707308cdf8f9bcfa1af4cb26586521e
    58/135:	https://codeload.github.com/pear/PEAR_Exception/legacy.zip/8c18719fdae000b690e3912be401c76e406dd13b
    59/135:	https://codeload.github.com/Masterminds/html5-php/legacy.zip/c961ca6a0a81dc6b55b6859b3f9ea7f402edf9ad
    60/135:	https://codeload.github.com/symfony/polyfill-iconv/legacy.zip/f037ea22acfaee983e271dd9c3b8bb4150bd8ad7
    61/135:	https://codeload.github.com/symfony/validator/legacy.zip/cc3f577d8887737df4d77a4c0cc6e3c22164cea4
    62/135:	https://codeload.github.com/njh/easyrdf/legacy.zip/acd09dfe0555fbcfa254291e433c45fdd4652566
    63/135:	https://codeload.github.com/grasmash/expander/legacy.zip/95d6037344a4be1dd5f8e0b0b2571a28c397578f
    64/135:	https://codeload.github.com/symfony/routing/legacy.zip/ff11aac46d6cb8a65f2855687bb9a1ac9d860eec
    65/135:	https://codeload.github.com/thephpleague/container/legacy.zip/43f35abd03a12977a60ffd7095efd6a7808488c0
    66/135:	https://codeload.github.com/consolidation/annotated-command/legacy.zip/004af26391cd7d1cd04b0ac736dc1324d1b4f572
    67/135:	https://codeload.github.com/consolidation/config/legacy.zip/11ab7ecd19131ce084390171b9c070eedecf7dab
    68/135:	https://codeload.github.com/grasmash/yaml-expander/legacy.zip/3f0f6001ae707a24f4d9733958d77d92bf9693b1
    69/135:	https://codeload.github.com/consolidation/output-formatters/legacy.zip/a942680232094c4a5b21c0b7e54c20cce623ae19
    70/135:	https://codeload.github.com/container-interop/container-interop/legacy.zip/79cbf1341c22ec75643d841642dd5d6acd83bdb8
    71/135:	https://codeload.github.com/composer/installers/legacy.zip/cfcca6b1b60bc4974324efb5783c13dca6932b5b
    72/135:	https://codeload.github.com/Chi-teck/drupal-code-generator/legacy.zip/a43131309b56a4c1874f39a9eaa4f6cb1a9832cd
    73/135:	https://codeload.github.com/consolidation/site-alias/legacy.zip/54ea74ee7dbd54ef356798028ca9a3548cb8df14
    74/135:	https://codeload.github.com/alchemy-fr/Zippy/legacy.zip/5ffdc93de0af2770d396bf433d8b2667c77277ea
    75/135:	https://codeload.github.com/dflydev/dflydev-dot-access-data/legacy.zip/3fbd874921ab2c041e899d044585a2ab9795df8a
    76/135:	https://codeload.github.com/dflydev/dflydev-dot-access-configuration/legacy.zip/2e6eb0c8b8830b26bb23defcfc38d4276508fc49
    77/135:	https://codeload.github.com/hechoendrupal/drupal-console-core/legacy.zip/bf1fb4a6f689377acec1694267f674178d28e5d1
    78/135:	https://codeload.github.com/stecman/symfony-console-completion/legacy.zip/bd07a24190541de2828c1d6469a8ddce599d3c5a
    79/135:	https://codeload.github.com/hechoendrupal/drupal-console-en/legacy.zip/ea956ddffab04f519a89858810e5f695b9def92b
    80/135:	https://codeload.github.com/php-fig/container/legacy.zip/b7ce3b176482dbbc1245ebf52b181af44c2cf55f
    81/135:	https://codeload.github.com/drush-ops/drush/legacy.zip/17f0106706391675a281c6d212850853bdbe90f9
    82/135:	https://codeload.github.com/webflo/drupal-finder/legacy.zip/8a7886c575d6eaa67a425dceccc84e735c0b9637
    83/135:	https://codeload.github.com/symfony/config/legacy.zip/c9bc510c217075d42d4a927e285917d0c2001cf4
    84/135:	https://codeload.github.com/hechoendrupal/drupal-console/legacy.zip/368bbfa44dc6b957eb4db01977f7c39e83032d18
    85/135:	https://codeload.github.com/webmozart/assert/legacy.zip/83e253c8e0be5b0257b881e1827274667c5c17a9
    86/135:	https://codeload.github.com/symfony/dependency-injection/legacy.zip/be0feb3fa202aedfd8d1956f2dafd563fb13acbf
    87/135:	https://codeload.github.com/symfony/translation/legacy.zip/301a5d627220a1c4ee522813b0028653af6c4f54
    88/135:	https://codeload.github.com/twigphp/Twig/legacy.zip/35889516bbd6bbe46a600c2c33b03515df4a076e
    89/135:	https://codeload.github.com/guzzle/promises/legacy.zip/a59da6cf61d80060647ff4d3eb2c03a2bc694646
    90/135:	https://codeload.github.com/hechoendrupal/drupal-console-extend-plugin/legacy.zip/f3bac233fd305359c33e96621443b3bd065555cc
    91/135:	https://codeload.github.com/guzzle/psr7/legacy.zip/9f83dded91781a01c63574e387eaa769be769115
    92/135:	https://codeload.github.com/webmozart/path-util/legacy.zip/d939f7edc24c9a1bb9c0dee5cb05d8e859490725
    93/135:	https://codeload.github.com/guzzle/guzzle/legacy.zip/407b0cb880ace85c9b63c5f9551db498cb2d50ba
    94/135:	https://codeload.github.com/symfony/finder/legacy.zip/7c0c627220308928e958a87c293108e5891cde1d
    95/135:	https://codeload.github.com/symfony/process/legacy.zip/a9c4dfbf653023b668c282e4e02609d131f4057a
    96/135:	https://codeload.github.com/php-fig/http-message/legacy.zip/f6561bf28d520154e4b0ec72be95418abe6d9363
    97/135:	https://codeload.github.com/ralouphie/getallheaders/legacy.zip/5601c8a83fbba7ef674a7369456d12f1e0d0eafa
    98/135:	https://codeload.github.com/symfony/yaml/legacy.zip/212a27b731e5bfb735679d1ffaac82bd6a1dc996
    99/135:	https://codeload.github.com/dnoegel/php-xdg-base-dir/legacy.zip/265b8593498b997dc2d31e75b89f053b5cc9621a
    100/135:	https://codeload.github.com/JakubOnderka/PHP-Console-Color/legacy.zip/d5deaecff52a0d61ccb613bb3804088da0307191
    101/135:	https://codeload.github.com/symfony/css-selector/legacy.zip/8ca29297c29b64fb3a1a135e71cb25f67f9fdccf
    102/135:	https://codeload.github.com/composer/semver/legacy.zip/46d9139568ccb8d9e7cdd4539cab7347568a5e2e
    103/135:	https://codeload.github.com/doctrine/common/legacy.zip/30e33f60f64deec87df728c02b107f82cdafad9d
    104/135:	https://codeload.github.com/drupal-composer/drupal-scaffold/legacy.zip/fc6bf4ceecb5d47327f54d48d4d4f67b17da956d
    105/135:	https://codeload.github.com/doctrine/inflector/legacy.zip/5527a48b7313d15261292c149e55e26eae771b0a
    106/135:	https://codeload.github.com/JakubOnderka/PHP-Console-Highlighter/legacy.zip/9f7a229a69d52506914b4bc61bfdb199d90c5547
    107/135:	https://codeload.github.com/doctrine/persistence/legacy.zip/3da7c9d125591ca83944f477e65ed3d7b4617c48
    108/135:	https://codeload.github.com/doctrine/collections/legacy.zip/d2ae4ef05e25197343b6a39bae1d3c427a2f6956
    109/135:	https://codeload.github.com/doctrine/event-manager/legacy.zip/a520bc093a0170feeb6b14e9d83f3a14452e64b3
    110/135:	https://codeload.github.com/symfony/dom-crawler/legacy.zip/d40023c057393fb25f7ca80af2a56ed948c45a09
    111/135:	https://codeload.github.com/doctrine/cache/legacy.zip/d768d58baee9a4862ca783840eca1b9add7a7f57
    112/135:	https://codeload.github.com/bobthecow/psysh/legacy.zip/9aaf29575bb8293206bb0420c1e1c87ff2ffa94e
    113/135:	https://codeload.github.com/doctrine/annotations/legacy.zip/53120e0eb10355388d6ccbe462f1fea34ddadb24
    114/135:	https://codeload.github.com/doctrine/lexer/legacy.zip/83893c552fd2045dd78aef794c31e694c37c0b8c
    115/135:	https://codeload.github.com/doctrine/reflection/legacy.zip/02538d3f95e88eb397a5f86274deb2c6175c2ab6
    116/135:	https://codeload.github.com/symfony/var-dumper/legacy.zip/2159335b452d929cbb9921fc4eb7d1bfed32d0be
    117/135:	https://codeload.github.com/consolidation/self-update/legacy.zip/a1c273b14ce334789825a09d06d4c87c0a02ad54
    118/135:	https://codeload.github.com/consolidation/log/legacy.zip/b2e887325ee90abc96b0a8b7b474cd9e7c896e3a
    119/135:	https://codeload.github.com/asm89/stack-cors/legacy.zip/c163e2b614550aedcf71165db2473d936abbced6
    120/135:	https://codeload.github.com/dflydev/dflydev-placeholder-resolver/legacy.zip/c498d0cae91b1bb36cc7d60906dab8e62bb7c356
    121/135:	https://codeload.github.com/cweagans/composer-patches/legacy.zip/2ec4f00ff5fb64de584c8c4aea53bf9053ecb0b3
    122/135:	https://codeload.github.com/php-fig/log/legacy.zip/6c001f1daafa3a3ac1d8ff69ee4db8e799a654dd
    123/135:	https://codeload.github.com/symfony/debug/legacy.zip/681afbb26488903c5ac15e63734f1d8ac430c9b9
    124/135:	https://codeload.github.com/symfony/filesystem/legacy.zip/b52454ec66fe5082b7a66a491339d1f1da9a5a0d
    125/135:	https://codeload.github.com/symfony/polyfill-mbstring/legacy.zip/fe5e94c604826c35a32fa832f35bd036b6799609
    126/135:	https://codeload.github.com/symfony/http-kernel/legacy.zip/586046f5adc6a08eaebbe4519ef18ad52f54e453
    127/135:	https://codeload.github.com/symfony/polyfill-php70/legacy.zip/bc4858fb611bda58719124ca079baff854149c89
    128/135:	https://codeload.github.com/paragonie/random_compat/legacy.zip/0a58ef6e3146256cc3dc7cc393927bcc7d1b72db
    129/135:	https://codeload.github.com/symfony/console/legacy.zip/15a9104356436cb26e08adab97706654799d31d8
    130/135:	https://codeload.github.com/symfony/polyfill-ctype/legacy.zip/82ebae02209c21113908c229e9883c419720738a
    131/135:	https://codeload.github.com/nikic/PHP-Parser/legacy.zip/5221f49a608808c1e4d436df32884cbc1b821ac0
    132/135:	https://codeload.github.com/symfony/event-dispatcher/legacy.zip/a088aafcefb4eef2520a290ed82e4374092a6dff
    133/135:	https://codeload.github.com/symfony/http-foundation/legacy.zip/fa02215233be8de1c2b44617088192f9e8db3512
    134/135:	https://codeload.github.com/consolidation/Robo/legacy.zip/d4805a1abbc730e9a6d64ede2eba56f91a2b4eb3
    135/135:	https://codeload.github.com/drupal/core/legacy.zip/5da135abcf5dbc0204e9a75c87381c7553a74d5e
    Finished: success: 135, skipped: 0, failure: 0, total: 135
Package operations: 137 installs, 0 updates, 0 removals
  - Installing cweagans/composer-patches (1.6.5): Loading from cache
  - Installing composer/installers (v1.6.0): Loading from cache
  - Installing zaporylie/composer-drupal-optimizations (1.1.0): Loading from cache
  - Installing symfony/polyfill-ctype (v1.11.0): Loading from cache
  - Installing composer/semver (1.5.0): Loading from cache
  - Installing drupal-composer/drupal-scaffold (2.5.4): Loading from cache
  - Installing symfony/yaml (v3.4.27): Loading from cache
  - Installing symfony/finder (v3.4.22): Loading from cache
  - Installing drupal/console-extend-plugin (0.9.2): Loading from cache
  - Installing paragonie/random_compat (v2.0.18): Loading from cache
  - Installing symfony/polyfill-php70 (v1.11.0): Loading from cache
  - Installing symfony/polyfill-mbstring (v1.11.0): Loading from cache
  - Installing symfony/http-foundation (v3.4.27): Loading from cache
  - Installing symfony/event-dispatcher (v3.4.27): Loading from cache
  - Installing psr/log (1.1.0): Loading from cache
  - Installing symfony/debug (v3.4.27): Loading from cache
  - Installing symfony/http-kernel (v3.4.27): Loading from cache
  - Installing asm89/stack-cors (1.2.0): Loading from cache
  - Installing symfony/console (v3.4.27): Loading from cache
  - Installing consolidation/log (1.1.1): Loading from cache
  - Installing symfony/filesystem (v3.4.22): Loading from cache
  - Installing consolidation/self-update (1.1.5): Loading from cache
  - Installing dflydev/placeholder-resolver (v1.0.2): Loading from cache
  - Installing doctrine/lexer (v1.0.1): Loading from cache
  - Installing doctrine/annotations (v1.6.1): Loading from cache
  - Installing doctrine/reflection (v1.0.0): Loading from cache
  - Installing doctrine/event-manager (v1.0.0): Loading from cache
  - Installing doctrine/collections (v1.6.1): Loading from cache
  - Installing doctrine/cache (v1.8.0): Loading from cache
  - Installing doctrine/persistence (1.1.1): Loading from cache
  - Installing doctrine/inflector (v1.3.0): Loading from cache
  - Installing doctrine/common (v2.10.0): Loading from cache
  - Installing symfony/dom-crawler (v3.4.27): Loading from cache
  - Installing symfony/css-selector (v3.4.27): Loading from cache
  - Installing symfony/var-dumper (v3.4.22): Loading from cache
  - Installing nikic/php-parser (v4.2.1): Loading from cache
  - Installing jakub-onderka/php-console-color (v0.2): Loading from cache
  - Installing jakub-onderka/php-console-highlighter (v0.4): Loading from cache
  - Installing dnoegel/php-xdg-base-dir (0.1): Loading from cache
  - Installing psy/psysh (v0.9.9): Loading from cache
  - Installing ralouphie/getallheaders (2.0.5): Loading from cache
  - Installing psr/http-message (1.0.1): Loading from cache
  - Installing guzzlehttp/psr7 (1.5.2): Loading from cache
  - Installing guzzlehttp/promises (v1.3.1): Loading from cache
  - Installing guzzlehttp/guzzle (6.3.3): Loading from cache
  - Installing webmozart/assert (1.4.0): Loading from cache
  - Installing webmozart/path-util (2.3.0): Loading from cache
  - Installing webflo/drupal-finder (1.1.0): Loading from cache
  - Installing twig/twig (v1.40.1): Loading from cache
  - Installing symfony/translation (v3.4.27): Loading from cache
  - Installing symfony/process (v3.4.27): Loading from cache
  - Installing psr/container (1.0.0): Loading from cache
  - Installing symfony/dependency-injection (v3.4.27): Loading from cache
  - Installing symfony/config (v3.4.22): Loading from cache
  - Installing stecman/symfony-console-completion (0.9.0): Loading from cache
  - Installing drupal/console-en (1.8.0): Loading from cache
  - Installing dflydev/dot-access-data (v1.1.0): Loading from cache
  - Installing dflydev/dot-access-configuration (v1.0.3): Loading from cache
  - Installing drupal/console-core (1.8.0): Loading from cache
  - Installing alchemy/zippy (0.4.3): Loading from cache
  - Installing drupal/console (1.8.0): Loading from cache
  - Installing container-interop/container-interop (1.2.0): Loading from cache
  - Installing league/container (2.4.1): Loading from cache
  - Installing grasmash/yaml-expander (1.4.0): Loading from cache
  - Installing consolidation/site-alias (1.1.11): Loading from cache
  - Installing consolidation/output-formatters (3.4.0): Loading from cache
  - Installing grasmash/expander (1.0.0): Loading from cache
  - Installing consolidation/config (1.2.0): Loading from cache
  - Installing consolidation/annotated-command (2.11.2): Loading from cache
  - Installing consolidation/robo (1.4.6): Loading from cache
  - Installing chi-teck/drupal-code-generator (1.28.0): Loading from cache
  - Installing drush/drush (9.5.2): Loading from cache
  - Installing easyrdf/easyrdf (0.9.1): Loading from cache
  - Installing egulias/email-validator (2.1.7): Loading from cache
  - Installing masterminds/html5 (2.6.0): Loading from cache
  - Installing pear/pear_exception (v1.0.0): Loading from cache
  - Installing pear/console_getopt (v1.4.2): Loading from cache
  - Installing pear/pear-core-minimal (v1.10.9): Loading from cache
  - Installing pear/archive_tar (1.4.7): Loading from cache
  - Installing stack/builder (v1.0.5): Loading from cache
  - Installing symfony/routing (v3.4.27): Loading from cache
  - Installing symfony-cmf/routing (1.4.1): Loading from cache
  - Installing symfony/class-loader (v3.4.27): Loading from cache
  - Installing symfony/polyfill-iconv (v1.11.0): Loading from cache
  - Installing symfony/psr-http-message-bridge (v1.2.0): Loading from cache
  - Installing symfony/serializer (v3.4.27): Loading from cache
  - Installing symfony/validator (v3.4.27): Loading from cache
  - Installing brumann/polyfill-unserialize (v1.0.3): Loading from cache
  - Installing typo3/phar-stream-wrapper (v2.1.0): Loading from cache
  - Installing vlucas/phpdotenv (v2.6.1): Loading from cache
  - Installing zendframework/zend-diactoros (1.8.6): Loading from cache
  - Installing zendframework/zend-stdlib (3.2.1): Loading from cache
  - Installing zendframework/zend-escaper (2.6.0): Loading from cache
  - Installing zendframework/zend-feed (2.12.0): Loading from cache
  - Installing behat/mink (dev-master d5ee350): Cloning d5ee350c40 from cache
  - Installing symfony/browser-kit (v4.2.8): Loading from cache
  - Installing behat/mink-browserkit-driver (1.3.3): Loading from cache
  - Installing instaclick/php-webdriver (1.4.5): Loading from cache
  - Installing behat/mink-selenium2-driver (dev-master 8684ee4): Cloning 8684ee4e63 from cache
  - Installing fabpot/goutte (v3.2.3): Loading from cache
  - Installing myclabs/deep-copy (1.9.1): Loading from cache
  - Installing phar-io/version (1.0.1): Loading from cache
  - Installing phar-io/manifest (1.0.1): Loading from cache
  - Installing phpdocumentor/reflection-common (1.0.1): Loading from cache
  - Installing phpdocumentor/type-resolver (0.4.0): Loading from cache
  - Installing phpdocumentor/reflection-docblock (4.3.1): Loading from cache
  - Installing theseer/tokenizer (1.1.2): Loading from cache
  - Installing sebastian/version (2.0.1): Loading from cache
  - Installing sebastian/environment (3.1.0): Loading from cache
  - Installing sebastian/code-unit-reverse-lookup (1.0.1): Loading from cache
  - Installing phpunit/php-token-stream (2.0.2): Loading from cache
  - Installing phpunit/php-text-template (1.2.1): Loading from cache
  - Installing phpunit/php-file-iterator (1.4.5): Loading from cache
  - Installing phpunit/php-code-coverage (5.3.2): Loading from cache
  - Installing phpunit/php-timer (1.0.9): Loading from cache
  - Installing sebastian/recursion-context (3.0.0): Loading from cache
  - Installing sebastian/exporter (3.1.0): Loading from cache
  - Installing doctrine/instantiator (1.2.0): Loading from cache
  - Installing phpunit/phpunit-mock-objects (5.0.10): Loading from cache
  - Installing sebastian/diff (2.0.1): Loading from cache
  - Installing sebastian/comparator (2.1.3): Loading from cache
  - Installing sebastian/global-state (2.0.0): Loading from cache
  - Installing sebastian/object-reflector (1.1.1): Loading from cache
  - Installing sebastian/object-enumerator (3.0.3): Loading from cache
  - Installing sebastian/resource-operations (1.0.0): Loading from cache
  - Installing squizlabs/php_codesniffer (3.4.2): Loading from cache
  - Installing symfony/phpunit-bridge (v3.4.27): Loading from cache
  - Installing phpspec/prophecy (1.8.0): Loading from cache
  - Installing phpunit/phpunit (6.5.14): Loading from cache
  - Installing mikey179/vfsstream (v1.6.6): Loading from cache
  - Installing justinrainbow/json-schema (5.2.8): Loading from cache
  - Installing jcalderonzumba/gastonjs (v1.2.0): Loading from cache
  - Installing jcalderonzumba/mink-phantomjs-driver (v0.3.3): Loading from cache
  - Installing drupal/core (8.7.0): Loading from cache
  - Installing drupal/coder (8.3.3): Cloning a33d3388fb from cache
  - Installing behat/mink-goutte-driver (v1.2.1): Loading from cache
  - Installing webflo/drupal-core-require-dev (8.7.0)
paragonie/random_compat suggests installing ext-libsodium (Provides a modern crypto API that can be used to generate random bytes.)
symfony/console suggests installing symfony/lock
doctrine/cache suggests installing alcaeus/mongo-php-adapter (Required to use legacy MongoDB driver)
symfony/var-dumper suggests installing ext-symfony_debug
psy/psysh suggests installing ext-pdo-sqlite (The doc command requires SQLite to work.)
psy/psysh suggests installing hoa/console (A pure PHP readline implementation. You'll want this if your PHP install doesn't already support readline or libedit.)
symfony/dependency-injection suggests installing symfony/expression-language (For using expressions in service container configuration)
symfony/dependency-injection suggests installing symfony/proxy-manager-bridge (Generate service proxies to lazy load them)
alchemy/zippy suggests installing guzzle/guzzle (To use the GuzzleTeleporter with Guzzle 3)
drupal/console suggests installing symfony/thanks (Thank your favorite PHP projects on Github using the CLI!)
consolidation/robo suggests installing henrikbjorn/lurker (For monitoring filesystem changes in taskWatch)
consolidation/robo suggests installing natxet/CssMin (For minifying CSS files in taskMinify)
consolidation/robo suggests installing patchwork/jsqueeze (For minifying JS files in taskMinify)
easyrdf/easyrdf suggests installing ml/json-ld (~1.0)
pear/archive_tar suggests installing ext-xz (Lzma2 compression support.)
symfony/routing suggests installing symfony/expression-language (For using expression matching)
symfony/class-loader suggests installing symfony/polyfill-apcu (For using ApcClassLoader on HHVM)
symfony/psr-http-message-bridge suggests installing nyholm/psr7 (For a super lightweight PSR-7/17 implementation)
symfony/serializer suggests installing psr/cache-implementation (For using the metadata cache.)
symfony/serializer suggests installing symfony/property-access (For using the ObjectNormalizer.)
symfony/serializer suggests installing symfony/property-info (To deserialize relations.)
symfony/validator suggests installing psr/cache-implementation (For using the metadata cache.)
symfony/validator suggests installing symfony/expression-language (For using the Expression validator)
symfony/validator suggests installing symfony/intl
symfony/validator suggests installing symfony/property-access (For accessing properties within comparison constraints)
zendframework/zend-feed suggests installing zendframework/zend-cache (Zend\Cache component, for optionally caching feeds between requests)
zendframework/zend-feed suggests installing zendframework/zend-db (Zend\Db component, for use with PubSubHubbub)
zendframework/zend-feed suggests installing zendframework/zend-http (Zend\Http for PubSubHubbub, and optionally for use with Zend\Feed\Reader)
zendframework/zend-feed suggests installing zendframework/zend-servicemanager (Zend\ServiceManager component, for easily extending ExtensionManager implementations)
zendframework/zend-feed suggests installing zendframework/zend-validator (Zend\Validator component, for validating email addresses used in Atom feeds and entries when using the Writer subcomponent)
behat/mink suggests installing behat/mink-zombie-driver (fast and JS-enabled headless driver for any app (requires node.js))
behat/mink suggests installing dmore/chrome-mink-driver (fast and JS-enabled driver for any app (requires chromium or google chrome))
phpunit/php-code-coverage suggests installing ext-xdebug (^2.5.5)
sebastian/global-state suggests installing ext-uopz (*)
phpunit/phpunit suggests installing ext-xdebug (*)
phpunit/phpunit suggests installing phpunit/php-invoker (^1.1)
Package phpunit/phpunit-mock-objects is abandoned, you should avoid using it. No replacement was suggested.
Generating autoload files
> DrupalProject\composer\ScriptHandler::createRequiredFiles
Create a sites/default/settings.php file with chmod 0666
Create a sites/default/files directory with chmod 0777
Starting landogatsbydrupal_nodejs_1 ... done
/var/www/.npm-global/bin/gatsby -> /var/www/.npm-global/lib/node_modules/gatsby-cli/lib/index.js

> gatsby-telemetry@1.0.9 postinstall /var/www/.npm-global/lib/node_modules/gatsby-cli/node_modules/gatsby-telemetry
> node src/postinstall.js

╔════════════════════════════════════════════════════════════════════════╗
║                                                                        ║
║   Gatsby has started collecting anonymous usage analytics              ║
║   to help improve Gatsby for all users.                                ║
║                                                                        ║
║   If you'd like to opt-out, you can use `gatsby telemetry --disable`   ║
║   To learn more, checkout https://gatsby.dev/telemetry                 ║
║                                                                        ║
╚════════════════════════════════════════════════════════════════════════╝
+ gatsby-cli@2.5.12
added 254 packages from 135 contributors in 9.385s
Killing landogatsbydrupal_nodejs_1 ... done
Starting landogatsbydrupal_nodejs_1 ... done
/var/www/.npm-global/bin/yarn -> /var/www/.npm-global/lib/node_modules/yarn/bin/yarn.js
/var/www/.npm-global/bin/yarnpkg -> /var/www/.npm-global/lib/node_modules/yarn/bin/yarn.js
+ yarn@1.15.2
added 1 package in 0.542s
Killing landogatsbydrupal_nodejs_1 ... done
Starting landogatsbydrupal_nodejs_1          ... done
Starting landogatsbydrupal_appserver_1 ... done
Creating landogatsbydrupal_database_1        ... done
Starting landogatsbydrupal_appserver_nginx_1 ... done
Waiting until database service is ready...
Waiting until appserver_nginx service is ready...
Waiting until appserver service is ready...
Waiting until nodejs service is ready...
Waiting until database service is ready...
Waiting until database service is ready...
Waiting until database service is ready...
Waiting until database service is ready...
Waiting until database service is ready...
Waiting until database service is ready...
Install Drupal with drush.

 // You are about to DROP all tables in your 'drupal8' database. Do you want to continue?: yes.

 [notice] Starting Drupal installation. This takes a while.
 [success] Installation complete.
The following module(s) will be enabled: jsonapi, serialization

 // Do you want to continue?: yes.

 [success] Successfully enabled: jsonapi, serialization
 [success] Successfully uninstalled: contact
Installing Gatsby with yarn.
yarn install v1.15.2
[1/4] Resolving packages...
[2/4] Fetching packages...
info fsevents@1.2.9: The platform "linux" is incompatible with this module.
info "fsevents@1.2.9" is an optional dependency and failed compatibility check. Excluding it from installation.
[3/4] Linking dependencies...
[4/4] Building fresh packages...
Done in 53.98s.
Building the Gatsby site from Drupal.
╔════════════════════════════════════════════════════════════════════════╗
║                                                                        ║
║   Gatsby has started collecting anonymous usage analytics              ║
║   to help improve Gatsby for all users.                                ║
║                                                                        ║
║   If you'd like to opt-out, you can use `gatsby telemetry --disable`   ║
║   To learn more, checkout https://gatsby.dev/telemetry                 ║
║                                                                        ║
╚════════════════════════════════════════════════════════════════════════╝
success open and validate gatsby-configs — 0.004 s
success load plugins — 0.339 s
success onPreInit — 0.003 s
success delete html and css files from previous builds — 0.004 s
success initialize cache — 0.005 s
success copy gatsby files — 0.011 s
success onPreBootstrap — 0.006 s
Starting to fetch data from Drupal

success source and transform nodes — 2.631 s
success building schema — 0.410 s
success createPages — 0.035 s
success createPagesStatefully — 0.022 s
success onPreExtractQueries — 0.001 s
success update schema — 0.018 s
success extract queries from components — 0.107 s
success run static queries — 0.148 s — 3/3 20.38 queries/second
success run page queries — 0.045 s — 13/13 292.02 queries/second
success write out page data — 0.018 s
success write out redirect data — 0.000 s

success Build manifest and related icons — 0.114 s
success onPostBootstrap — 0.120 s

info bootstrap finished - 5.894 s

success Building production JavaScript and CSS bundles — 5.876 s
success Building static HTML for pages — 0.798 s — 13/13 66.35 pages/second
info Done building in 12.574 sec

BOOMSHAKALAKA!!!

Your app has started up correctly.
Here are some vitals:

 NAME                  lando-gatsby-drupal
 LOCATION              /home/benji/Sites/lando-gatsby-drupal
 SERVICES              appserver_nginx, appserver, database, nodejs
 APPSERVER_NGINX URLS  https://localhost:34025
                       http://localhost:34026
                       http://drupal.lgd.lndo.site:8000
                       https://drupal.lgd.lndo.site
                       http://gatsbydrupal.lgd.lndo.site:8000
                       https://gatsbydrupal.lgd.lndo.site
 NODEJS URLS           https://localhost:34024
                       http://gatsby.lgd.lndo.site:8000
                       https://gatsby.lgd.lndo.site
```

Once Lando has started, you should get something like this when you ask for
more info:

```
$ lando info
[
  {
    service: 'appserver_nginx',
    urls: [
      'https://localhost:34025',
      'http://localhost:34026',
      'http://drupal.lgd.lndo.site:8000',
      'https://drupal.lgd.lndo.site',
      'http://gatsbydrupal.lgd.lndo.site:8000',
      'https://gatsbydrupal.lgd.lndo.site'
    ],
    type: 'nginx',
    webroot: '.',
    config: {},
    version: '1.14',
    meUser: 'www-data',
    hostnames: [
      'appserver_nginx.landogatsbydrupal.internal'
    ]
  },
  {
    service: 'appserver',
    urls: [],
    type: 'php',
    via: 'nginx',
    served_by: 'appserver_nginx',
    webroot: 'drupal/web',
    config: {
      php: '/home/benji/.lando/config/drupal8/php.ini',
      vhosts: '/home/benji/.lando/config/drupal8/default.conf.tpl'
    },
    version: '7.2',
    meUser: 'www-data',
    hostnames: [
      'appserver.landogatsbydrupal.internal'
    ]
  },
  {
    service: 'database',
    urls: [],
    type: 'mariadb',
    internal_connection: {
      host: 'database',
      port: '3306'
    },
    external_connection: {
      host: 'localhost',
      port: '34023'
    },
    creds: {
      database: 'drupal8',
      password: 'drupal8',
      user: 'drupal8'
    },
    config: {
      database: '/home/benji/.lando/config/drupal8/mysql.cnf'
    },
    version: '10.1',
    meUser: 'www-data',
    hostnames: [
      'database.landogatsbydrupal.internal'
    ]
  },
  {
    service: 'nodejs',
    urls: [
      'https://localhost:34024',
      'http://gatsby.lgd.lndo.site:8000',
      'https://gatsby.lgd.lndo.site'
    ],
    type: 'node',
    config: {},
    version: '10',
    meUser: 'node',
    hostnames: [
      'nodejs.landogatsbydrupal.internal'
    ]
  }
]
```
